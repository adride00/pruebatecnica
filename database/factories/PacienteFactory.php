<?php

namespace Database\Factories;

use App\Models\Paciente;
use Illuminate\Database\Eloquent\Factories\Factory;

class PacienteFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Paciente::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombres' => $this->faker->name,
            'apellidos' => $this->faker->lastName,
            'dui' => $this->faker->numerify('##########'),
            'email' => $this->faker->email,
            'fecha_nacimiento' => $this->faker->date('Y-m-d'),
            'nacionalidad' => $this->faker->randomElement(['salvadoreña', 'hondureña', 'nicaraguense']),
            'telefono_fijo' => $this->faker->numerify('########'),
            'telefono_movil' => $this->faker->numerify('########'),
            'departamento' => $this->faker->randomElement(['San Miguel', 'San Salvador', 'La Union', 'Santa Ana', 'Cabañas', 'La Libertad']),
            'municipio' => $this->faker->randomElement(['Ilobasco', 'San Salvador', 'Santa Tecla', 'San Miguel', 'Ilopango']),
            'direccion' => $this->faker->address,
            'ocupacion' => $this->faker->jobTitle,
            'estado_civil' => $this->faker->randomElement(['Soltero', 'Casado', 'Divorsiado']),
            'nombre_emergencia' => $this->faker->name,
            'telefono_emergencia' => $this->faker->numerify('########'),
            'parentesco' => $this->faker->randomElement(['padre', 'madre', 'tio', 'sobrino']),
            'created_at' => now()

        ];
    }
}
