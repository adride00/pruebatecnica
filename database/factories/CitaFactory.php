<?php

namespace Database\Factories;

use App\Models\Cita;
use App\Models\Paciente;
use Illuminate\Database\Eloquent\Factories\Factory;

class CitaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Cita::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id_paciente' => Paciente::all()->random()->id,
            'fecha' => $this->faker->date('Y-m-d'),
            'hora' => $this->faker->time('H:i:s'),
            'durecion' => $this->faker->numerify('##'),
            'estado' => $this->faker->randomElement(['Confirmado', 'No confirmado', 'canceldao', 'atendido']),
            'motivo_consulta' => $this->faker->words(10, true),
            'observaciones' => $this->faker->words(10, true),
            'id_usuario' => $this->faker->randomElement([1, 2, 3]),
            'created_at' => $this->faker->date('Y-m-d')
        ];
    }
}
