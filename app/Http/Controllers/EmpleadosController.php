<?php

namespace App\Http\Controllers;

use App\Models\Departamento;
use App\Models\Empleado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class EmpleadosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empleados = DB::table('empleados')
                ->join('departamentos', 'departamentos.id', '=', 'empleados.idDepartamento')
                ->select('empleados.*', 'departamentos.*')
                ->get();
      
        return view('empleado.index')->with('empleados', $empleados);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departamentos = Departamento::all();
        return view("empleado.create")->with('departamentos', $departamentos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $empleado = new Empleado();
        $empleado->NomEmp = $request->get('nombres');
        $empleado->ApellEmp = $request->get('apellidos');
        $empleado->Telefono = $request->get('telefono');
        $empleado->Correo = $request->get('email');
        $empleado->idDepartamento = $request->get('departamento');

        $empleado->save();
        return redirect('empleados');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $departamentos = Departamento::all();
        $empleado = DB::table('empleados')
        ->join('departamentos', 'departamentos.id', '=', 'empleados.idDepartamento')
        ->select('empleados.*', 'departamentos.*')
        ->where('empleados.id', '=', $id)
        ->get();
        // dd($empleado[0]);
        return view('empleado.edit', ['empleado' => $empleado, 'departamentos' => $departamentos]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $empleado = Empleado::find($id);

        $empleado->NomEmp = $request->get('nombres');
        $empleado->ApellEmp = $request->get('apellidos');
        $empleado->Telefono = $request->get('telefono');
        $empleado->Correo = $request->get('email');
        $empleado->idDepartamento = $request->get('departamento');

        $empleado->save();
        return redirect('empleados');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $empleado = Empleado::find($id);
        $empleado->delete();
        return redirect('empleados');
    }

}
