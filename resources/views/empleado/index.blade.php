@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Administracion Empleado</h1>
@stop

@section('content')
<div class="card">
    <div class="card-header">
        <a class="btn alert-info" href="empleados/create">
            <i class="fa fa-plus"></i> Agregar Empleado
          </a>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <table class="table table-bordered">
        <thead>                  
          <tr>
            <th style="width: 20px">Id Empleado</th>
            <th style="width: 20px">Nombre</th>
            <th style="width: 20px">Correo</th>
            <th style="width: 40px">Departamento</th>
            <th style="width: 70px">Controles</th>
          </tr>
        </thead>
        <tbody>
            @foreach($empleados as $empleado)

                <tr>
                <td>{{ $empleado->id }}</td>
                <td>{{ $empleado->NomEmp }} {{ $empleado->ApellEmp }}</td>
                <td>{{ $empleado->Correo }}</td>
                <td>{{ $empleado->NomDepartamento }}</td>
                <td>
                
                    <div class="container position-relative">
                        <div class="row position-absolute top-0 start-50 translate-middle">
                            
                            <form action="{{ route('empleados.destroy', $empleado->id)}}" method="POST">
                              @csrf
                              @method('DELETE')
                                <a href="/empleados/{{$empleado->id}}/edit" class="btn btn-social-icon btn-bitbucket"><i class="fa fa-pen"></i></a>
                                <button type="submit" class="btn btn-social-icon btn-bitbucket"><i class="fa fa-trash-alt"></i></button>
                            </form>     
                        </div>  
                    </div>
                   
                </td>
                
                </tr>        
            
                @endforeach
        </tbody>
      </table>
    </div>
    <!-- /.card-body -->
  </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop