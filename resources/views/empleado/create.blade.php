@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Nuevo Empleado</h1>
@stop
@section('content')

<div class="card">
    <h5 class="card-header">Información Personal</h5>
    <div class="card-body">
    
        <form action="/empleados" method="POST">
            @csrf
            @include('empleado.formularioEmpleado')
        </form>
  </div>

@stop