@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Nuevo Empleado</h1>
@stop
@section('content')

<div class="card">
    <h5 class="card-header">Información Personal</h5>
    <div class="card-body">
    
        <form action="/empleados/{{$empleado[0]->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="nombres">Nombres</label>
                        <input type="text" class="form-control" id="nombres" name="nombres" value={{$empleado[0]->NomEmp}} placeholder="Nombres">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="apellidos">Apellidos</label>
                        <input type="text" class="form-control" id="apellidos" name="apellidos" placeholder="Apellidos" value={{$empleado[0]->ApellEmp}}>
                    </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="nombres">Correo</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email" value={{$empleado[0]->Correo}}>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="apellidos">Telefono</label>
                        <input type="text" class="form-control" id="telefono" name="telefono" placeholder="Telefono" value={{$empleado[0]->Telefono}}>
                    </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="nombres">Departamento</label>
                        <select class="form-control" name="departamento" id="departamento" placeholder="Departamento">
                            <option value="">-- Departamento -- </option>
                            @foreach($departamentos as $departamento)
                                <option value="{{ $departamento['id'] }}">{{ $departamento['NomDepartamento'] }}</option>
                            @endforeach
                        </select>
                    </div>
                  </div>
              </div>
            
            </div>
            <div class="card-footer text-muted">
                <a href="/empleados" class="btn btn-secondary" tabindex="5">Cancelar</a>
                <button type="submit" class="btn btn-success" tabindex="4">Guardar</button>
            </div>
        </form>
  </div>

@stop